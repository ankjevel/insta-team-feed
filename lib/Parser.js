module.exports = (function () {
  'use strict';

  var Q = require('q');
  var request = require('request');
  var parseString = require('xml2js').parseString;

  function Parser () {
    this.options = function (url, contentType, timeout) {
      return {
        "timeout": timeout || 3000,
        "uri": url,
        "headers": {
          "Content-type": contentType || 'text/xml'
        }
      };
    };
  }

  Parser.prototype.getXMLLink = function (link) {
    var parser = this;
    var deferred = Q.defer();
    var options = parser.options(link);
    request(options, function (error, response, body) {

      function fail() {
        console.error(error);
        deferred.reject(json);
      }
      var json = {
        "data": body,
        "error": {
          "reqError": error,
          "uri": link
        }
      };
      if (!response) {
        return fail();
      }
      var status = response.statusCode;
      if (error ||
          status !== 200 ||
          response.headers['content-type'].indexOf('xml') === -1) {
        return fail();
      }
      return deferred.resolve(parser.parse(json));
    });
    return deferred.promise;
  };

  Parser.prototype.parse = function (object) {
    var deferred = Q.defer();
    if (typeof object !== 'object') {
      deferred.resolve(object);
    } else {
      var link = object.error && object.error.uri ? object.error.uri : "";
      var reqError = object.error && object.error.reqError ? object.error.reqError : null;
      parseString(object.data, function (error, result) {
        var json = {
          "data": null,
          "error": {
            "reqError": reqError,
            "parseError": error,
            "link": link
          }
        };
        if (error) {
          console.error(error);
          return deferred.reject(json);
        }
        try {
          var channel = result.rss.channel[0];
          var items = channel.item;
          json.data = items.slice(0);
        } catch (parseError) {
          console.error(parseError);
          json.data = result;
        }
        return deferred.resolve(json);
      });
    }
    return deferred.promise;
  };

  return new Parser();

})();