(function () {
  'use strict';
  var Express = require('express');
  var app = new Express();
  var port = 8090;

  var URL = require('url');
  var fs = require('fs');

  var Q = require('q');
  var Saved = require(__dirname + '/lib/Saved');
  var Parser = require(__dirname + '/lib/Parser');
  var usersFile = __dirname + '/lib/users.json';

  // setup
  app.configure(function() {
    app
    .use(function (req, res, next) {
      var origin = req.headers.origin;
      origin = origin ? origin : '*';
      res.removeHeader('X-Powered-By');
      res.charset = 'utf-8';
      res.header('Access-Control-Allow-Origin', origin);
      res.header('Access-Control-Allow-Methods', 'GET,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Accept,Origin,Referer,User-Agent');
      if (req.method === 'OPTIONS') {
        res.send(200);
      } else {
        next();
      }
    })
    .set('trust proxy', true)
    .use(Express.logger(':date - :req[X-Forwarded-For] - ":method :url" ":referrer" :response-time ms'))
    .use(Express.bodyParser())
    .use(Express.methodOverride())
    .use(app.router)
    .use(Express.static(__dirname + '/public'))
    .use(Express.errorHandler({
      dumpExceptions: false,
      showStack: false
    }))
    .listen(port);
  })

  .get('/', function (req, res) {
    res.sendfile(__dirname + '/static/index.htm');
  })
  .options('/*', function (req, res) {
    return res.json({});
  })

  .get('/iteam', function (req, res) {
    var count = parseInt(req.query.count, 10) || 8;

    var prev = Saved.get({
      "id": "iteamse",
      "count": count
    });

    if (prev) {
      return res.json(prev);
    }

    fs.readFile(usersFile, function (error, file) {
      if (error) {
        if (error.errno === 34) {
          fs.appendFile(usersFile, '[]');
        }
        console.error(error);
        return res.send(null);
      }
      try {
        file = JSON.parse(file.toString());
      } catch (parseError) {
        console.error(parseError);
        file = [];
      }
      var deferred = Q.defer();

      var users = file.map(function (userURL) {
        return Parser.getXMLLink(userURL).then(function (promise) {
          return promise;
        }, function (error) {
          return error;
        });
      });

      var content = {
        "data": [],
        "status": 200,
        "error": null
      };

      Q.all(users).then(function (promise) {

        promise.forEach(function (xmlContent) {
          if (xmlContent.data) {
            if (xmlContent.data instanceof Array) {
              xmlContent.data.forEach(function (data) {
                content.data.push(data);
              });
            } else {
              content.data.push(xmlContent.data);
            }
          }
          if (xmlContent.error && xmlContent.error.parseError || xmlContent.error.reqError) {
            if (content.error === null) {
              content.error = [];
            }
            content.error.push(xmlContent.error);
          }
        });

        content.data.sort(function (a, b) {
          var aa = new Date(a && a.pubDate ? a.pubDate[0] : null);
          var bb = new Date(a && a.pubDate ? b.pubDate[0] : null);
          if (aa > bb) {
            return -1;
          } else if (bb > aa) {
            return 1;
          }
          return 0;
        });

        content.data = Saved.set({
          "data": content.data,
          "id": "iteamse",
          "count": count,
          "error": content.error
        });

        content.status = 200;
        res.json(content);
      });
      return deferred.promise;
    });
  })

  .get('/rss', function (req, res) {
    var url = req.query.url;

    var count = parseInt(req.query.count, 10) || 3;

    if (!url) {
      return res.send(405);
    }

    var parsed = URL.parse(url);
    if (!parsed.host || !parsed.path) {
      return res.send(406);
    }

    var id = (parsed.host + parsed.path).toLowerCase().replace(/[\W]/g, '');

    var prev = Saved.get({
      "id": id,
      "count": count
    });

    if (prev) {
      return res.json(prev);
    }

    var json = {
      "data": [],
      "status": 200,
      "error": null
    };

    Parser.getXMLLink(url).then(function (promise) {
      try {
        var items = promise.data;
        var error = null;
        if (promise.error && promise.error.parseError || promise.error.reqError) {
          error = promise.error;
        }
        json.data = Saved.set({
          "data": items.slice(0),
          "id": id,
          "count": count,
          "error": error
        });
      } catch (parseError) {
        console.error(parseError);
        json.data = result;
      }
      res.json(json);
    }, function (error) {
      json.error = [error];
      json.status = 500;
      res.json(json);
    });
  })

  .get('/*', function (req, res) {
    res.redirect('/');
  });

  console.log('started on port %s', port);

})();